from math import pi
from Figura import Figura

class Circulo(Figura):
	radio = 0
	def __init__(self,radio):
		self.radio = radio

	def Area(self):
		# hacer potenciacion en python
		# base ** exponente
		# pow(base,exponente)
		# formula de area pi * radio ^ 2
		return pi * pow(self.radio,2)
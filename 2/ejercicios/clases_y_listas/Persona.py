class Persona(object):
	nombre = ''
	edad   = ''
	sexo   = 'H'
	peso   = 0
	altura = 0

	def __init__(self, nombre,edad,sexo,peso,altura):
		self.nombre = nombre
		self.edad   = edad
		self.sexo   = sexo
		self.peso   = peso
		self.altura = altura

	def calcularIMC(self):
		return (self.peso/pow(self.altura,2))

	def valoracionDePeso(self):
		imc = self.calcularIMC()
		retorno = 0
		if imc >= 18 and imc < 25:
			retorno = -1 # peso normal
		elif imc >= 25 and imc < 27:
			retorno = 0 # sobre peso
		elif imc > 27:
			retorno = 1 # obesidad 
		else :
			retorno = -2 # cualquier otra valoracion
		return retorno
	
	def mayorEdad(self):
		return self.edad >= 18
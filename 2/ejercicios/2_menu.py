import os
from  menu_modulo.Triangulo import Triangulo
from  menu_modulo.Cuadrado import Cuadrado
from  menu_modulo.Circulo import Circulo
resp = 'y'
while resp == 'Y' or resp == 'y':
#    os.system('clear')
    print(" ************************* MENU ************************* ")
    print("1 - Triangulo ")
    print("2 - Cuadrado ")
    print("3 - Circulo ")
    op = 0
    while op < 1 or op > 3:
        op = int(raw_input("Digame su opcion preferida [1-3] -> :  "))
    if op == 1:
        base = int(raw_input("dame la base :  "))
        altura = int(raw_input("dame la altura:  "))
        triangulo = Triangulo(base,altura);
        print " El area del Triangulo es : %.2f " % triangulo.Area()
    if op == 2:
        lado = int(raw_input("dame el valor de un lado :  "))
        cuadrado = Cuadrado(lado);
        print " El area del Cuadrado es : %.2f " % cuadrado.Area()
    if op == 3:
        radio = float(raw_input("dame el valor del radio :  "))
        circulo = Circulo(radio);
        print " El area del Circulo es : %.2f " % circulo.Area()
    resp = raw_input("Desea repetir el menu: (y/n) ")
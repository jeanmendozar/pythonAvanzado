import os
from  calculadora.mi_clase import Calculadora
resp = 'y'
while resp == 'Y' or resp == 'y':
    os.system('clear')

    arg1 = float(raw_input("deme el argumento 1 :  "))
    arg2 = float(raw_input("dame el argumento 2 :  "))
    bandera = True
    while bandera:
        print(" ************************* MENU ************************* ")
        print("1 - Suma")
        print("2 - Division ")
        print("3 - Potenticia")
        print("4 - Raiz Cuadrada")
        print("5 - Actualizar Argumentos")
        print("6 - Salir")

        op = 0
        while op < 1 or op > 6:
            op = int(raw_input("Digame su opcion preferida [1-6] -> :  "))

        calc = Calculadora(arg1,arg2)

        if op == 1:
            print " la suma de (%.2f+%.2f) =  %.2f  " % (arg1,arg2,calc.Suma())
        if op == 2:
            if arg2 == 0:
                print "No puedo hacer divisiones entre CERO "
            else:
                print " la division de (%.2f/%.2f) =  %.2f " % (arg1,arg2,calc.Division())
        if op == 3:
            print " la potencia es (%.2f^%.2f) = %.2f " % (arg1,arg2,calc.Potencia())
        if op == 4:
            print " la raiz cuadrada es (%.2f) = %.2f " % (arg1,calc.RaizCuadrada())
        if op == 5 :
            bandera = False
        if op == 6 :
            exit()
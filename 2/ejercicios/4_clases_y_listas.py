import os
from  clases_y_listas.Persona import Persona
resp = 'y'
while resp == 'Y' or resp == 'y':
    #os.system('clear')
    nombre = raw_input("Nombre:  ")
    edad   = int(raw_input("Edad:  "))
    sexo   = raw_input("SEXO (H/M):  ")
    peso   = float(raw_input("Peso:  "))
    altura = float(raw_input("Altura:  "))
    persona = Persona(nombre,edad,sexo,peso,altura)
    bandera = True
    while bandera:
        print(" ************************* MENU ************************* ")
        print("1 - IMC")
        print("2 - Valoracion")
        print("3 - Mayor de edad ?")
        print("4 - Actualizar Persona")
        print("5 - Salir")

        op = 0
        while op < 1 or op > 5:
            op = int(raw_input("Digame su opcion preferida [1-5] -> :  "))

        if op == 1:
            imc = persona.calcularIMC()
            print (" Su IMC %.2f " % imc)
        if op == 2:
            valoracion = persona.valoracionDePeso()
            if valoracion == -1:
                valoracion = 'Normal'
            elif valoracion == 0:
                valoracion = 'Sobrepeso'
            elif valoracion == 1:
                valoracion = 'Obesidad'
            elif valoracion == -2:
                valoracion = 'No lo se'
            print (" Su Peso es : %s " % valoracion )
        if op == 3:
            es_mayor = persona.mayorEdad()
            print (" e mayor de edad : -> %s" % es_mayor)
        if op == 4 :
            bandera = False
        if op == 5 :
            exit()
from django.conf.urls import url

import apps.principal.views as principal 


urlpatterns =  [
    url(r'^$',principal.index,name="home"),
    url(r'^projects/$',principal.projectsIndex, name ="list_projects" ),
    url(r'^delete_project/$', principal.projectsDelete ,name = "delete_project" ),
    url(r'^projects/(?P<pk>[0-9]+)/edit/$', principal.projectsEdit, name='projects_edit'),
]

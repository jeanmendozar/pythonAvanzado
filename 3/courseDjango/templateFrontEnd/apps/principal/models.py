from django.db import models

# Create your models here.

STATUS_CHOICES = (
    ('be', 'Begin'),
    ('pr', 'Progress'),
    ('sc', 'Success'),
)

class Member(models.Model):
	nombre = models.CharField(max_length=100)
	apellido = models.CharField(max_length=100)
	def __str__(self):
		return "%s %s " %(self.nombre,self.apellido)

class Project(models.Model):
	nombre = models.CharField(max_length=100)
	date_create = models.DateField()
	members = models.ManyToManyField(Member, verbose_name="list of members")
	progress = models.IntegerField(default=0)
	status =  models.CharField(max_length=2, choices=STATUS_CHOICES)

	def __str__(self):
		return self.nombre



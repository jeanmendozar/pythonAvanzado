from django.shortcuts import render,redirect,get_object_or_404
from django.http import HttpResponse
import json
from apps.principal.models import Project
from .forms import ProjectForm
# Create your views here.

def index(request):
	return render(request,"principal/index.html")

def projectsIndex(request):
	list_projects = Project.objects.all();
	return render(request,"principal/projects.html",{"projects_list":list_projects})

def projectsDelete(request):
	if request.is_ajax():
		if request.method == 'POST':
			print(request.POST.get('pk_project'))
			project = Project.objects.get(pk=request.POST.get('pk_project')).delete()
			response_data = {}
			response_data['msg'] = 'Projects Eliminado.'
			return HttpResponse(
	            json.dumps(response_data),
	            content_type="application/json"
	        )
	else:
		return HttpResponse(
            json.dumps({"msj": "No puedes estar aqui"}),
            content_type="application/json"
        )

def projectsEdit(request, pk):
    project = get_object_or_404(Project, pk=pk)
    if request.method == "POST":
        form = ProjectForm(request.POST, instance=project)
        if form.is_valid():
        	form.save()
        	return redirect('projects_edit', pk=project.pk)
            #project = form.save(commit=False)
            #project.date_update = now() request.user
            #project.save()
    else:
        form = ProjectForm(instance=project)
    return render(request, 'principal/project_edit.html', {'form': form})


def projectsNew(request):
	if request.method == "POST":
		form = ProjectForm(request.POST)
		if form.is_valid():
			project = form.save(commit=False)
			project.save()
			#ProjectForm.author = request.user
			#ProjectForm.published_date = timezone.now()
			return redirect('projects_edit', pk=project.pk)
		else:
			form = ProjectForm()
	return render(request, 'principal/project_edit.html', {'form': form})
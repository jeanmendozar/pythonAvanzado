from django.contrib import admin
from apps.principal.models import Member,Project


# Register your models here.

admin.site.register(Member)
admin.site.register(Project)

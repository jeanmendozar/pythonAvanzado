from django.contrib import admin

from myApp.models import Reporter,Article

# Register your models here.

admin.site.register(Reporter)
admin.site.register(Article)

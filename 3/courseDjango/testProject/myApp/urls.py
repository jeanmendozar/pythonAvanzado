from django.conf.urls import url

from  myApp import views

urlpatterns =  [
    url(r'^$',views.home, name ='home'),
    url(r'^comotellamas/$',views.sellama, name ='comoTeLLamas'),
]

from django.conf.urls import url

import apps.basic.views as basic


urlpatterns =  [
    url(r'^$',basic.index , name = 'home'),
	url(r'^hola/$',basic.hola , name = 'hola')
]
